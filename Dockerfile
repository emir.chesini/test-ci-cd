FROM node:18.1.0 AS build

WORKDIR /app

# Instalar las dependencias del proyecto
COPY ./package.json ./package.json
COPY ./package-lock.json ./package-lock.json
RUN npm install

# Copiar los archivos del proyecto
COPY . ./

# Construir el proyecto de Angular
RUN npm run build

# Imagen final de docker que se utilizará en el despliegue
FROM nginx:1.13.9-alpine
COPY --from=build /app/dist/test-ci-cd/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]